filetype plugin indent on
syntax on
set number
set relativenumber
set hidden
set ruler
let mapleader=","
inoremap <leader>g <esc>:w<cr>a
xnoremap <leader>g :w<cr>
nnoremap <leader>g :w<cr>
vnoremap <leader>g :w<cr>
 
set laststatus=2
inoremap <space><space> <esc>
xnoremap <space><space> <esc>
nnoremap <space><space> <esc>
vnoremap <space><space> <esc>

" make a change and don't save
" this should reload your vimrc every time you save
autocmd! bufwritepost .vimrc source ~/.vimrc
