[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
alias d-php='docker run -it --rm -v $(pwd):/opt/project -v ~/.ssh:/root/.ssh miamioh/php:7.1-phpstorm bash'
