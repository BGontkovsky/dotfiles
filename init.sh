#!/usr/bin/env zsh

# Assume some form of zsh is installed. This won't always be true, especially on
# linux.
#
# Anywhere you see /usr/local/bin/* would have to be edited on a Linux system.
# Most Linux systems strongly favor /usr/bin/ for binaries, especially ones like
# zsh.
#
# Configuration files like these usually have a . (dot) at the beginning of
# their names. In this repo they don't because MacOS makes it unnecessarily
# difficult to edit hidden files (dotfiles). Instead, we're letting the files be
# visible in the repo, and symlinking them into place as hidden files in $HOME.
#
# Any configuration files for your shell, editors, or other programs that use
# dotfiles can be added to this directory without a leading dot and they'll be
# symlinked into your $HOME directory when you run this script. I would track
# your .gitconfig in this repo. Copy it here with (assuming you're in this
# directory):
#
# cp $HOME/.gitconfig ./gitconfig
#
# then run this script and it will be added back to your home directory. To see
# what I track in my dotfiles see https://gitlab.com/yramagicman/stow-dotfiles.
# Things are structured differently there, so you'll find most of my settings in
# the config directory.
#
# Use zsh settings. Zsh is capable of behaving like several different shells
# depending on what your settings are. We want it to behave like itself
emulate zsh
# Install homebrew. If ever ported to Linux, this would need to be avoided with
# a conditional to such as:
# if [[ $OSTYPE ~= "darwin*" ]]; then ...
if [[ ! -z $(command -v brew > /dev/null 2>&1) ]]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

# Install iTerm if not already installed
if [[ ! -f "/Applications/iTerm.app" ]]; then
    brew cask install iterm2
fi

# see http://zsh.sourceforge.net/Doc/Release/Conditional-Expressions.html for an
# explanation of how zsh conditional syntax works.
# This is also mac specific. Most Linux distros have zsh installed at
# /usr/bin/zsh. Again, this could be hidden behind a conditional.
if [[ "$(which zsh)" != '/usr/local/bin/zsh' ]]; then
    brew install zsh
    if [[ ! -z grep -q '/usr/local/bin/zsh' /etc/shells ]]; then
        echo 'adding up-to date zsh to /etc/shells. need sudo password.'
        echo '/usr/local/bin/zsh' | sudo tee -a /etc/shells
    fi
fi
# Switch the shell to zsh for all future sessions.
if [[ $SHELL != '/usr/local/bin/zsh' ]]; then
    echo 'Changing login shell for the current user. This requires your password'
    chsh -s '/usr/local/bin/zsh'
fi
# Make fancy regex and substitutions work natively in the shell
setopt EXTENDED_GLOB
# Make sure prezto exists in the repo. By using submodules we can ensure that
# you always have what you need. Maintenance is a little bit more complicated
# this way, but it makes porting to other systems easier.
#
# To update prezto you'll have to do:
#
# cd $ZPREZTODIR
# git pull
# git submodule update --init --recursive
#
# Then cd back out into your dotfiles repo and run:
#
# git submodule update --init --recursive
#
# Then commit your dotfiles.
if [[ ! -d "${0:h}/.zprezto" && ! -d "${0:h}/.git/modules/prezto" ]]; then
    git submodule add --name prezto https://github.com/sorin-ionescu/prezto "${0:h}/.zprezto"
fi
# Making sure prezto is here, even if the repo is cloned without the --recursive
# flag.
git submodule update --init --recursive
# Because we want to track settings in each of these files we're copying them
# out of the prezto repo instead of using a symlink like the install directions
# recommend on the prezto repository website.
for rcfile in "$PWD/${0:h}"/zprezto/runcoms/^README.md(.N); do
    # But we're only copying the files if they don't already exist.
    if [[ ! -f "${0:h}/.${rcfile:t}" ]]; then
        cp -v "$rcfile" "${0:h}/.${rcfile:t}"
    fi
done
# for every file in just this directory
# that does not contain .git at the beginning of the name
# and is not init.sh
find ./  -maxdepth 1 \
    -not -name '.' \
    -not -name '.tags' \
    -not -name 'init.sh' | while read -r file
    do
        # Create a symlink to your home directory. Like the previous symlink,
        # this will fail if any of the files already exist. We don't want to
        # lose anything you already have.
        ln -svf "$PWD/${file:t}" "$HOME/${file:t}"
    done
# Load zsh, replacing the current shell with zsh. The exec command here makes
# sure you don't end up with multiple nested shells, causing a small issue when
# trying to exit via the exit or quit commands.
exec zsh -l